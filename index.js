import { AppRegistry } from 'react-native';
import Client from 'client';

const App = Client;

AppRegistry.registerComponent('callscore', () => App);
