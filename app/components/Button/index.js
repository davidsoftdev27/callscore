import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { noop } from 'lodash';

const styles = {
  button: {
    height: 60,
    backgroundColor: 'lightgray',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'darkgray',
    flex: 1,
    textAlign: 'center',
  },
};

const Button = ({ onPress, children }) => (
  <TouchableOpacity onPress={onPress} style={styles.button}>
    {typeof children === 'string'
      ? (<Text style={styles.buttonText}>{children}</Text>)
      : children
    }
  </TouchableOpacity>
);

Button.propTypes = {
  children: PropTypes.node,
  onPress: PropTypes.func,
};

Button.defaultProps = {
  children: '',
  onPress: noop,
};

export default Button;
