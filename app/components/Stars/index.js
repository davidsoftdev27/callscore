import React from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import { View, TouchableOpacity } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const styles = {
  stars: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
};

const stars = new Array(5).fill(0).map((num, index) => index + 1);

const Stars = ({ score, onPress }) => (
  <View style={styles.stars}>
    {
      stars.map(num => (
        <TouchableOpacity key={num} onPress={() => onPress(num)}>
          <FontAwesome
            name={num > score ? 'star-o' : 'star'}
            size={50}
            style={{ color: num > score ? 'rgb(230, 190, 0)' : 'rgb(230, 190, 0)' }}
          />
        </TouchableOpacity>
      ))
    }
  </View>
);

Stars.propTypes = {
  score: PropTypes.number,
  onPress: PropTypes.func,
};

Stars.defaultProps = {
  score: 0,
  onPress: noop,
};

export default Stars;
