import React from 'react';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const styles = {
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },
};

const Content = ({ children, style }) => (
  <KeyboardAwareScrollView
    style={{ ...styles.container, ...style }}
  >
    {children}
  </KeyboardAwareScrollView>
);

Content.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.shape(),
};

Content.defaultProps = {
  style: {},
};

export default Content;
