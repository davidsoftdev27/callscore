import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
};

const Footer = ({ children }) =>
  <View style={styles.container}>{children}</View>;

Footer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Footer;
