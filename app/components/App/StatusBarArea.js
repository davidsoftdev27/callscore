import React from 'react';
import { View } from 'react-native';

const styles = {
  container: {
    width: '100%',
    height: 20,
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
  },
};

const StatusBarArea = () => <View style={styles.container} />;

export default StatusBarArea;
