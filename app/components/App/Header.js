import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Ionicons from 'react-native-vector-icons/Ionicons';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    height: 80,
    alignItems: 'center',
    backgroundColor: '#3366aa',
  },
  text: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 20,
  },
  menus: {
    position: 'absolute',
    right: 10,
    flexDirection: 'row',
  },
  menu: {
    padding: 5,
  },
};

const menus = [
  { path: 'welcome', icon: 'md-list' },
  { path: 'call-rating', icon: 'md-star' },
  { path: 'incoming-call', icon: 'md-call' },
  { path: 'settings', icon: 'md-settings' },
];

const Header = ({ children, justifyContent, activePath }) => {
  if (!children) return null;
  const containerStyle = {
    ...styles.container,
    justifyContent,
  };
  const menusView = (
    <View style={styles.menus}>
      {
        menus.map(menu => (
          <TouchableOpacity
            onPress={() => {
              if (menu.push) {
                Actions[menu.path]();
              } else {
                Actions.reset(menu.path, { stars: parseInt(Math.random() * 5, 10) });
              }
            }}
            disabled={menu.path === activePath}
            style={{
              ...styles.menu,
              opacity: menu.path === activePath ? 1 : 0.7,
            }}
            key={menu.path}
          >
            <Ionicons name={menu.icon} size={20} color="white" />
          </TouchableOpacity>
        ))
      }
    </View>
  );
  if (typeof children === 'string') {
    return (
      <View style={containerStyle}>
        <Text style={styles.text}>{children}</Text>
        {menusView}
      </View>
    );
  }
  return (
    <View style={containerStyle}>
      {children}
      {menusView}
    </View>
  );
};

Header.propTypes = {
  activePath: PropTypes.string,
  children: PropTypes.node,
  justifyContent: PropTypes.string,
};

Header.defaultProps = {
  activePath: '',
  children: '',
  justifyContent: 'center',
};

export default Header;
