import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

import StatusBarArea from './StatusBarArea';

const styles = {
  container: {
    paddingTop: 20,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    backgroundColor: 'white',
  },
};

class App extends Component {
  static contextTypes = {
    tokenInfo: PropTypes.shape({
      id: PropTypes.string,
      userId: PropTypes.number,
    }),
  };

  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  state = { sideBarOpen: false };

  onToggle = () => this.setState({ sideBarOpen: !this.state.sideBarOpen });

  render() {
    const { children } = this.props;
    return (
      <View style={styles.container}>
        <StatusBarArea />
        {children}
      </View>
    );
  }
}

export default App;
