
import React from 'react';
import { Provider } from 'react-redux';
import { fromJS } from 'immutable';

import configureStore from 'redux/store';
import I18n from 'components/I18n';

import Router from 'router';

const initialState = fromJS({});
const store = configureStore(initialState);

const Client = () => (
  <Provider store={store}>
    <I18n>
      <Router />
    </I18n>
  </Provider>
);

export default Client;
