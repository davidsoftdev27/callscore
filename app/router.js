import React from 'react';
import PropTypes from 'prop-types';
import { compose, setDisplayName } from 'recompose';
import { connect } from 'react-redux';
import { AsyncStorage, View } from 'react-native';
import { Scene, Stack, Router as FluxRouter } from 'react-native-router-flux';
import CallDetectorMangager from 'react-native-call-detection';
import RNCallKit from 'react-native-callkit';

import { FIRST_SCREEN_NAME } from 'constants';
import { injectIntl } from 'components/Intl';
import { dispatchMaker } from 'helpers/redux-connect';
import { setTokenInfo } from 'redux/auth/actions';
import { getDataSelector } from 'redux/selectors';
import immutableToJS from 'helpers/immutableToJS';

import CallRating from 'routes/CallRating';
import IncomingCall from 'routes/IncomingCall';
import Settings from 'routes/Settings';
import Splash from 'routes/Splash';
import Welcome from 'routes/Welcome';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    width: '100%',
    backgroundColor: 'white',
  },
};

class Router extends React.Component {
  static propTypes = {
    formatMessage: PropTypes.func.isRequired,
    setTokenInfo: PropTypes.func.isRequired,
    tokenInfo: PropTypes.shape({
      email: PropTypes.string,
      phone: PropTypes.string,
      password: PropTypes.string,
    }).isRequired,
  }
  static childContextTypes = {
    formatMessage: PropTypes.func,
    tokenInfo: PropTypes.shape({
      email: PropTypes.string,
      phone: PropTypes.string,
      password: PropTypes.string,
    }),
  }

  state = { loading: true, initial: 'splash', event: false };

  getChildContext() {
    const { formatMessage, tokenInfo } = this.props;
    return {
      formatMessage,
      tokenInfo,
    };
  }

  componentWillMount() {
    // TODO: we need to improve the following to make a logic for login
    AsyncStorage.getItem('tokenInfo', (err, tokenInfo) => {
      if (!err && tokenInfo) {
        const tokenInfoObject = JSON.parse(tokenInfo);
        this.props.setTokenInfo({ data: tokenInfoObject });
        if (tokenInfoObject.email && tokenInfoObject.password && tokenInfoObject.phone) {
          this.setState({ initial: FIRST_SCREEN_NAME, loading: false });
          return;
        }
      }
      this.setState({ initial: 'settings', loading: false });
    });
    console.log('bbb');
    /* this.callDetector = new CallDetectorMangager((event) => {
      if (event === 'Incoming') {

      }
      console.log('calldetectormanager1', event);
    }); */
    try {
      RNCallKit.setup({
        appName: 'callscore',
      });
    } catch (error) {
      console.log('error:', error.message);
    }
    console.log(RNCallKit);
    RNCallKit.addEventListener('didReceiveStartCallAction', this.callkitEvent);
    RNCallKit.addEventListener('answerCall', this.callkitEvent);
    RNCallKit.addEventListener('endCall', this.callkitEvent);
    RNCallKit.addEventListener('didDisplayIncomingCall', this.callkitEvent);
  }
  callkitEvent = () => this.setState({ event: true });
  render() {
    const { loading, initial, event } = this.state;
    if (loading) return <View />;
    const scenes = [
      { key: 'call-rating', component: CallRating },
      { key: 'incoming-call', component: IncomingCall },
      { key: 'settings', component: Settings },
      { key: 'splash', component: Splash },
      { key: 'welcome', component: Welcome },
    ];
    if (event) return <View />;
    return (
      <View style={styles.container}>
        <FluxRouter>
          <Stack key="root">
            {
              scenes.map(scene => (
                <Scene
                  key={scene.key}
                  component={scene.component}
                  initial={initial === scene.key}
                  hideNavBar
                />
              ))
            }
          </Stack>
        </FluxRouter>
      </View>
    );
  }
}

/* istanbul ignore next */
const mapStateToProps = state => ({
  tokenInfo: getDataSelector('auth', 'tokenInfo')(state),
});

export default compose(
  setDisplayName('Router'),
  connect(
    mapStateToProps,
    dispatchMaker([
      [setTokenInfo, 'setTokenInfo'],
    ]),
  ),
  immutableToJS,
  injectIntl,
)(Router);
