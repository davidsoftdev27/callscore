import generateHandleActions from 'redux/state-handler';
import { LOGIN, SET_TOKEN_INFO } from 'redux/constants';

const apiStates = [
  { type: LOGIN, name: 'tokenInfo' },
];
const instantStates = [
  {
    type: SET_TOKEN_INFO, name: 'tokenInfo', kind: 'object', defaultValue: { data: {} },
  },
];
const storage = {
  tokenInfo: 'tokenInfo',
};
const listValues = [];

export default generateHandleActions({
  apiStates, instantStates, storage, listValues,
});
