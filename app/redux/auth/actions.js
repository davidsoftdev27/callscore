import { createApiAction, createAction, createClearAction } from 'redux/redux-actions';

import restApis from '../restApis';
import { LOGIN, SET_TOKEN_INFO } from '../constants';

const loginApi = restApis('login');

export const login = createApiAction(LOGIN, loginApi.create);
export const clearTokenInfo = createClearAction(LOGIN);
export const setTokenInfo = createAction(SET_TOKEN_INFO);
