import { combineReducers } from 'redux-immutable';

import auth from 'redux/auth/reducer';
import ui from 'redux/ui/reducer';

const rootReducer = combineReducers({
  auth,
  ui,
});

export default rootReducer;
