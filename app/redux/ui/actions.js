import { createAction, createClearAction } from 'redux/redux-actions';
import {
  SET_HEADER_TITLE,
  CLEAR_HEADER_TITLE,
} from 'redux/constants';

export const setHeaderTitle = createAction(SET_HEADER_TITLE);
export const clearHeaderTitle = createClearAction(CLEAR_HEADER_TITLE);
