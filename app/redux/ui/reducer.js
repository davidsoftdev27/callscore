import generateHandleActions from 'redux/state-handler';
import {
  SET_HEADER_TITLE,
  CLEAR_HEADER_TITLE,
} from 'redux/constants';

const apiStates = [];
const instantStates = [
  { type: SET_HEADER_TITLE, name: 'headerTitle' },
  { type: CLEAR_HEADER_TITLE, name: 'headerTitle' },
];
const listValues = [];

export default generateHandleActions({ apiStates, instantStates, listValues });
