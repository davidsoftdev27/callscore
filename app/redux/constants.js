// Auth
export const LOGIN = 'auth/LOGIN';
export const SET_TOKEN_INFO = 'auth/SET_TOKEN_INFO';

// ui
export const GLOBAL_NOTIFICATION = 'ui/GLOBAL_NOTIFICATION';
export const SET_HEADER_TITLE = 'ui/SET_HEADER_TITLE';
export const CLEAR_HEADER_TITLE = 'ui/CLEAR_HEADER_TITLE';
