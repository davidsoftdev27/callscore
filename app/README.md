# Includes

Source Files

## Folders / Files

### Tree
+assets  
+components  
+redux  
+routes  
+styles  
+translations  
client.js  
router.js  

### Files Description

`client.js` has a topmost rendering function to render react app into a dom element.  
Here we import everything (router, store, ...) and use them to wrap the application.  
`router.js` has all the scenes and keys defined in it.
