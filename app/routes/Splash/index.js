import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';

import App from 'components/App';
import Content from 'components/App/Content';

const Splash = (props, { formatMessage }) => (
  <App isLoggedIn={false}>
    <Content>
      <Text>This is a splash screen(src/routes/Splash/index.js)</Text>
      <TouchableHighlight onPress={() => Actions.reset('login')}>
        <Text>{formatMessage('Login with email')}</Text>
      </TouchableHighlight>
    </Content>
  </App>
);

Splash.contextTypes = {
  formatMessage: PropTypes.func,
};

export default Splash;
