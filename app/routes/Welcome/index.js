import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

import App from 'components/App';
import Content from 'components/App/Content';
import Header from 'components/App/Header';

const Welcome = () => (
  <App isLoggedIn={false}>
    <Header activePath="welcome">WELCOME</Header>
    <Content>
      <Text>This is a welcome screen(src/routes/Welcome/index.js)</Text>
    </Content>
  </App>
);

Welcome.contextTypes = {
  formatMessage: PropTypes.func,
};

export default Welcome;
