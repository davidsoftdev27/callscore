import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, TextInput, View } from 'react-native';

import App from 'components/App';
import Content from 'components/App/Content';
import Header from 'components/App/Header';
import Footer from 'components/App/Footer';
import Button from 'components/Button';

const styles = {
  inputContainer: {
    flex: 1,
    padding: 10,
  },
  textInputContainer: {
    marginBottom: 15,
  },
  actionsContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  text: {
    fontSize: 17,
    padding: 10,
    flex: 1,
    fontWeight: 'bold',
    color: 'darkgray',
  },
  textInput: {
    fontSize: 20,
    padding: 10,
    flex: 1,
  },
};

class Settings extends Component {
  static contextTypes = {
    formatMessage: PropTypes.func,
    tokenInfo: PropTypes.shape({
      email: PropTypes.string,
      phone: PropTypes.string,
      password: PropTypes.string,
    }),
  };

  state = { ...this.context.tokenInfo };

  onChangeTextHandler = name => (value) => {
    this.setState({ [name]: value });
  }

  render() {
    const { formatMessage } = this.context;
    const { email, password, phone } = this.state;
    return (
      <App isLoggedIn={false}>
        <Header activePath="settings">
          {formatMessage('SETINGS')}
        </Header>
        <Content>
          <View style={styles.inputContainer}>
            <View style={styles.textInputContainer}>
              <Text style={styles.text}>{formatMessage('EMAIL ADDRESS')}</Text>
              <TextInput
                value={email}
                onChangeText={this.onChangeTextHandler('email')}
                style={styles.textInput}
                keyboardType="email-address"
                placeholder="user@callscore.com"
              />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.text}>{formatMessage('PHONE NUMBER')}</Text>
              <TextInput
                value={phone}
                onChangeText={this.onChangeTextHandler('phone')}
                style={styles.textInput}
                keyboardType="phone-pad"
                placeholder="1234567890"
              />
            </View>
            <View style={styles.textInputContainer}>
              <Text style={styles.text}>{formatMessage('PASSWORD')}</Text>
              <TextInput
                value={password}
                onChangeText={this.onChangeTextHandler('password')}
                style={styles.textInput}
                secureTextEntry
                placeholder="password"
              />
            </View>
          </View>
        </Content>
        <Footer>
          <View style={styles.actionsContainer}>
            <Button>{formatMessage('CANCEL')}</Button>
            <Button>{formatMessage('SAVE')}</Button>
          </View>
        </Footer>
      </App>
    );
  }
}

export default Settings;
