import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';

import App from 'components/App';
import Content from 'components/App/Content';
import Header from 'components/App/Header';
import Footer from 'components/App/Footer';
import Button from 'components/Button';
import Stars from 'components/Stars';

const styles = {
  container: {
    flex: 1,
    padding: 50,
  },
  text: {
    fontSize: 25,
    marginBottom: 50,
  },
  stars: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  actionsContainer: {
    flex: 1,
    flexDirection: 'row',
  },
};

class CallRating extends Component {
  static contextTypes = {
    formatMessage: PropTypes.func,
    tokenInfo: PropTypes.shape({
      email: PropTypes.string,
      phone: PropTypes.string,
      password: PropTypes.string,
    }),
  };

  state = { score: 0 };

  onChangeTextHandler = name => (value) => {
    this.setState({ [name]: value });
  }

  render() {
    const { formatMessage } = this.context;
    const { score } = this.state;
    return (
      <App isLoggedIn={false}>
        <Header activePath="call-rating">
          {formatMessage('CALL RATING')}
        </Header>
        <Content>
          <View style={styles.container}>
            <Text style={styles.text}>{formatMessage('Please let us know what you think of this caller. Choose a rating - more stars for a better call!')}</Text>
            <Stars onPress={num => this.setState({ score: num })} score={score} />
          </View>
        </Content>
        <Footer>
          <View style={styles.actionsContainer}>
            <Button>{formatMessage('DONE')}</Button>
          </View>
        </Footer>
      </App>
    );
  }
}

export default CallRating;
