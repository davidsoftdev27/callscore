import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

import App from 'components/App';
import Content from 'components/App/Content';
import Header from 'components/App/Header';
import Footer from 'components/App/Footer';
import Button from 'components/Button';
import Stars from 'components/Stars';

const styles = {
  topContainer: {
    flex: 1,
    paddingHorizontal: 50,
    paddingVertical: 20,
    alignItems: 'center',
  },
  middleContainer: {
    paddingHorizontal: 20,
  },
  infoGroup: {
    marginBottom: 30,
  },
  infoLabel: {
    color: 'gray',
    fontSize: 18,
  },
  info: {
    fontSize: 25,
    marginLeft: 30,
  },
  actionsContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  actionWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: 80,
    height: 80,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  declineButton: {
    backgroundColor: '#FF6666',
  },
  acceptButton: {
    backgroundColor: '#66FF66',
  },
  title: {
    fontSize: 30,
  },
  text: {
    color: 'gray',
    fontSize: 17,
    marginVertical: 15,
  },
};

class IncomingCall extends Component {
  static contextTypes = {
    formatMessage: PropTypes.func,
  };

  static propTypes = {
    stars: PropTypes.number,
  };

  static defaultProps = {
    stars: 0,
  };

  onAccept = () => {
    Actions.pop();
  };

  onDecline = () => {
    Actions.pop();
  };

  render() {
    const { formatMessage } = this.context;
    const { stars } = this.props;
    return (
      <App isLoggedIn={false}>
        <Header activePath="incoming-call">
          {formatMessage('CALL ON HOLD')}
        </Header>
        <Content>
          <View style={styles.topContainer}>
            <Stars score={stars} />
            <Text style={styles.title}>{stars} {formatMessage(stars === 1 ? 'Star Call' : 'Stars Call')}</Text>
          </View>
          <View style={styles.middleContainer}>
            <View style={styles.infoGroup}>
              <Text style={styles.infoLabel}>{formatMessage('CALLER NUMBER')}</Text>
              <Text style={styles.info}>972-498-9899</Text>
            </View>
            <View style={styles.infoGroup}>
              <Text style={styles.infoLabel}>{formatMessage('CALLER NAME')}</Text>
              <Text style={styles.info}>Joseph Blow</Text>
            </View>
            <View style={styles.infoGroup}>
              <Text style={styles.infoLabel}>{formatMessage('REPEAT CALLER')}</Text>
              <Text style={styles.info}>Yes</Text>
            </View>
          </View>
        </Content>
        <Footer>
          <View style={styles.actionsContainer}>
            <Button>{formatMessage('NO, NOT NOW')}</Button>
            <Button>{formatMessage('YES, SEND IT')}</Button>
          </View>
        </Footer>
      </App>
    );
  }
}

export default IncomingCall;
