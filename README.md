# Project Title

CallScore Frontend Application

## Getting Started

Please follow the below instruction get it running.

### Prerequisites

```
node
```

### Installing

```
npm install -g yarn
yarn
```

### Running

```
Use expo to run
```

### Testing
```
yarn lint // Run eslint (all js/jsx files)
yarn lint-fix // Fix linting issues
yarn test // Do unit testing
```

### Translate
```
yarn translate
```

### Deployment

The project is stored in github and connected with Semaphore
If the master branch is updated, it is auto-deployed to staging site.
If the production branch is updated, it goes to production site.
